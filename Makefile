all: kin

kin: kin.cpp
	g++ -Wall -o kin kin.cpp -g

clean:
	rm kin *.o

remake:
	clean
	all

.PHONY:
	remake clean all
