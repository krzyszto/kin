#include <stdio.h>
#include <algorithm>
#include <vector>

int k, M, rozmiar_drzewa;
const int MOD = 1000000000; //10^9

int potega_dwojki(int a)
{
	int pocz = 1;
	while (pocz < a + 1)
		pocz *= 2;
	return pocz;
}

int query(int a, int b, int pos, int **drzewo)
{
	//fprintf(stderr, "wywoalnie query dla przedzialu od %i do %i na pos %i\n", a, b, pos);
	if (pos == 0) {
		return 1;
	}
	//printf("query od %i do %i na pos %i\n", a, b, pos);
	int va = M + a, vb = M + b;
	/* Skrajne przedziały do rozkładu. */
	//printf("okej\n");
	int wyn = drzewo[va][pos];

	if (va != vb) {
		wyn += drzewo[vb][pos];
	}
	//printf("tu ok\n");
	/* Spacer aż do momentu spotkania. */
	while (va / 2 != vb / 2) {
		if (va % 2 == 0) {
			wyn += drzewo[va + 1][pos]; /* prawa bombka na lewej ścieżce */
			wyn %= MOD;
		}
		if (vb % 2 == 1) {
			wyn += drzewo[vb - 1][pos]; /* lewa bombka na prawej ścieżce */
			wyn %= MOD;
		}
		va /= 2; vb /= 2;
	}
	//fprintf(stderr, "W przedziale od %i do %i na pozycji %i jest razem %i\n", a, b, pos, wyn);
	return wyn;
}

void wstaw(int v, int **drzewo)
{
	for (int i = k; i > 0; i--) {
		//fprintf(stderr, "wstaw dla pozycji %i dla wierzcholka %i\n", i,
		drzewo[v][i] += query(v - M, M - 1, i-1, drzewo);//drzewo[v][i-1];
		drzewo[v][i] %= MOD;
	}
}

void zaktualizujOjca(int ojciec, int **drzewo)
{
	for (int i = 1; i <= k; i++) {
	//	printf("tato sie zmienil\n");
		drzewo[ojciec][i] = (drzewo[2 * ojciec][i] +
					drzewo[2 * ojciec + 1][i]) % MOD;
	}
}

void insert(int x, int **drzewo) { /* val==1 to wstawianie, val==-1 to usuwanie */
	int v = M + x;
	//w[v] += val;
	wstaw(v, drzewo);
	while (v != 1) {
		v /= 2;
		zaktualizujOjca(v, drzewo);
	}
}

/**
 * Ustawia wezlowi drzewa poczatkowe wartosci.
 **/

void initWezel(int wierzcholek, int **drzewo)
{
	for (int i = 1; i <= k; i++) {
		drzewo[wierzcholek][i] = 0;
	}
	drzewo[wierzcholek][0] = 1;
}


/**
 * Tworzy drzewo przedzialowe na tablicy alokowanej na stercie.
 **/
int** zrob(const int n, const int a)
{
	int **tmp = new int*[n];
	for (int i = 0; i < n; i++) {
		tmp[i] = new int[a];
	}
	for (int i = 0; i < n; i++) {
		initWezel(i, tmp);
	}
	return tmp;
}

void usun(int **drzewo, const int n, const int k)
{
	for (int i = 0; i < n; i++) {
		delete[] drzewo[i];
	}
	delete[] drzewo;
}

int main()
{
	int n;
	scanf("%i%i", &n, &k);
	rozmiar_drzewa = potega_dwojki(n) * 2;
	M = potega_dwojki(n);
	int **drzewo = zrob(rozmiar_drzewa, k + 1);
	for (int i = 0; i < n; i++) {
		int tmp;
		scanf("%i", &tmp);
		insert(tmp, drzewo);
	}
	printf("%i\n", drzewo[1][k]);
	usun(drzewo, rozmiar_drzewa, k);
	return 0;
}
